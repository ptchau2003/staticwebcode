package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
  @Test
  public void testAdd() throws Exception {
    int res = new Calculator().add();
    assertEquals("add", 9, res);
  
  }
 @Test
  public void testSub() throws Exception {
    int res = new Calculator().sub();
    assertEquals("sub", 3, res);

  }
}
